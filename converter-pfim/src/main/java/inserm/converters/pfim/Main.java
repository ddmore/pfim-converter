/* ----------------------------------------------------------------------------
 * This file is part of PharmML to PFIM 4.0 Converter.  
 * 
 * Copyright (C) 2016 jointly by the following organisations:- 
 * 1. INSERM, Paris, France
 * 2. Universite Paris Diderot, Paris, France
 * 3. EMBL European Bioinformatics Institute (EBML-EBI), Hinxton, UK
 * 4. Cyprotex Discovery Ltd, Macclesfield, England, UK
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation. A copy of the license agreement is provided
 * in the file named "LICENSE.txt" included with this software distribution.
 * ----------------------------------------------------------------------------
 */ 

package inserm.converters.pfim;

import eu.ddmore.convertertoolbox.api.response.ConversionDetail;
import eu.ddmore.convertertoolbox.api.response.ConversionDetail.Severity;
import eu.ddmore.convertertoolbox.api.response.ConversionReport;
import eu.ddmore.convertertoolbox.api.response.ConversionReport.ConversionCode;
import gnu.getopt.Getopt;
import gnu.getopt.LongOpt;

import java.io.File;
import java.util.List;

import crx.converter.engine.ConversionDetail_;

/**
 * <p>Command-line wrapper for the PFIM converter.</p>
 * <p><strong>Command-line options:-</strong></p>
 * <dl>
 * <dt>-p directory_path</dt>
 * <dd>Path to the PFIM program directory where the PFIM software is installed</dd>
 * <dt>-i filepath</dt>
 * <dd>Filepath to the input PharmML</dd>
 * <dt>-o directory_path</dt>
 * <dd>Path to the output directory to create output PFIM R files</dd>
 * </dl> 
 */
public class Main {
	public static boolean echoMessages = true;
	public static void main(String[] args) throws Exception {
		// Command-line options
		File in = null;
		File output_dir = null;
		String program_directory = null;
		
		Converter c_ = new Converter();
		int c;
		LongOpt[] longopts = new LongOpt[4];
		Getopt g = new Getopt("inserm.converters.pfim.Main", args, "p:i:o:");
		StringBuffer sb = new StringBuffer();
		int i = 0;
		longopts[i++] = new LongOpt("program", LongOpt.OPTIONAL_ARGUMENT, sb, 'p');
		longopts[i++] = new LongOpt("in", LongOpt.OPTIONAL_ARGUMENT, sb, 'i');
		longopts[i++] = new LongOpt("output", LongOpt.OPTIONAL_ARGUMENT, sb, 'o');
		
		while ((c = g.getopt()) != -1) {
			switch (c) {
			case (int) 'p':
				program_directory = g.getOptarg();
				program_directory = program_directory.replace("\\", "/");
				break;
			case (int) 'i':
				in = new File(g.getOptarg());
				break;
			case (int) 'o':
				output_dir = new File(g.getOptarg());
				break;
			default:
				continue;
			}
		}
		
		if (program_directory == null) throw new NullPointerException("PFIM program directory not specified.");
		if (in == null) throw new NullPointerException("Input PharmML file not specified");
		if (output_dir == null) throw new NullPointerException("Output directory not specified.");
		
		Parser p = (Parser) c_.getParser();
		p.setProgramDirectory(program_directory);
		
		ConversionReport report = c_.performConvert(in, output_dir);
		if (report.getReturnCode() == ConversionCode.FAILURE) {
			System.err.println("WARNING: Conversion failed.");
			throw new UnsupportedOperationException();
		}
		
		List<ConversionDetail> details = report.getDetails(Severity.INFO);
		ConversionDetail_ detail = (ConversionDetail_) details.get(0);
		System.out.println("Output File: " + detail.getFile());
	}
}
