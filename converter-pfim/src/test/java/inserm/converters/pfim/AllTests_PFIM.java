/* ----------------------------------------------------------------------------
 * This file is part of PharmML to PFIM 4.0 Converter.  
 * 
 * Copyright (C) 2016 jointly by the following organisations:- 
 * 1. INSERM, Paris, France
 * 2. Universite Paris Diderot, Paris, France
 * 3. EMBL European Bioinformatics Institute (EBML-EBI), Hinxton, UK
 * 4. Cyprotex Discovery Ltd, Macclesfield, England, UK
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation. A copy of the license agreement is provided
 * in the file named "LICENSE.txt" included with this software distribution.
 * ----------------------------------------------------------------------------
 */

package inserm.converters.pfim;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
	Test_Simeoni_ShortWin2.class,
	Test_Warafin_PK_ODE.class,
	Test_Continuous_Covariate_Checking.class,
	Test_Analytical_Dose_Target.class,
	Test_Warafin_PK_Analytical.class,
	Test_Warafin_PK_ODE_optFW.class,
	Test_Set_Algorithm.class,
	Test_Set_Bad_Algorithm.class,
	Test_Warafin_PK_ODE_optSIMP.class,
	Test_Warafin_PKPD_Turnover.class,
	Test_Warafin_PKPD_optFW.class,
	Test_Warafin_PK_BOV.class,
	Test_Warafin_PK_COV.class,
	Test_Usecase1c.class,
	Test_Usecase1c_old.class,
	Test_Usecase5.class
})
public class AllTests_PFIM {

}
