/* ----------------------------------------------------------------------------
 * This file is part of PharmML to PFIM 4.0 Converter.  
 * 
 * Copyright (C) 2016 jointly by the following organisations:- 
 * 1. INSERM, Paris, France
 * 2. Universite Paris Diderot, Paris, France
 * 3. EMBL European Bioinformatics Institute (EBML-EBI), Hinxton, UK
 * 4. Cyprotex Discovery Ltd, Macclesfield, England, UK
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation. A copy of the license agreement is provided
 * in the file named "LICENSE.txt" included with this software distribution.
 * ----------------------------------------------------------------------------
 */

package inserm.converters.pfim;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.Date;

/**
 * I/O Handler to run 'R' jobs via a Java-based system call.
 */
public class RJob {
	private String interpreterExePath = "";
	private String format = "";
	public static class InputStreamHandler extends Thread {
		private InputStream stream;
		private StringBuffer buffer = new StringBuffer();
		private volatile boolean exitLoop = false;

		InputStreamHandler(InputStream stream_) {
			stream = stream_;
			start();
		}

		public void run() {
			try {
				int nextChar;
				while ((nextChar = stream.read()) != -1) {
					if (exitLoop) break;
					buffer.append((char) nextChar);
				}
				stream.close();
			} catch (IOException e) {
			}
		}
		
		public void close() {
			exitLoop = true;
		}
		
		public String getBuffer() {
			return buffer.toString();
		}
	}
	
	public void setInterpreterExePath(String path) {
		interpreterExePath = path;
	}
	
	public void setCommandFormat(String format_) {
		format = format_;
	}
	
	public boolean run(File run_dir, File script_file) throws IOException, InterruptedException {
		String exe_cmd = String.format(format, interpreterExePath);
		String[] cmd = { exe_cmd, "CMD", "BATCH", script_file.getAbsolutePath() };
		Runtime rt = Runtime.getRuntime();
		Process proc = rt.exec(cmd, null, run_dir);
		
		InputStreamHandler stdout  = new InputStreamHandler(proc.getInputStream());
		InputStreamHandler stderr  = new InputStreamHandler(proc.getErrorStream());
		int exitVal = proc.waitFor();
		stdout.close();
		stderr.close();
		
		Date now = new Date();
		String stdout_file = run_dir.getAbsolutePath() + File.separatorChar + "stdout.txt"; 
		PrintWriter stdout_txt = new PrintWriter(stdout_file);
		stdout_txt.write(stdout.getBuffer());
		stdout_txt.write("\n\nDated: " + now + "\n\n");
		stdout_txt.close();
		
		String stderr_file = run_dir.getAbsolutePath() + File.separatorChar + "stderr.txt"; 
		PrintWriter stderr_txt = new PrintWriter(stderr_file);
		stderr_txt.write(stderr.getBuffer());
		stderr_txt.write("\n\nDated: " + now + "\n\n");
		stderr_txt.close();
		
		if (exitVal != 0) throw new InterruptedException("Program call interupted...");
		
		return true;
	}
}
