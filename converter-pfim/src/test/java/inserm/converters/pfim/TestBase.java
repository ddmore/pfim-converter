/* ----------------------------------------------------------------------------
 * This file is part of PharmML to PFIM 4.0 Converter.  
 * 
 * Copyright (C) 2016 jointly by the following organisations:- 
 * 1. INSERM, Paris, France
 * 2. Universite Paris Diderot, Paris, France
 * 3. EMBL European Bioinformatics Institute (EBML-EBI), Hinxton, UK
 * 4. Cyprotex Discovery Ltd, Macclesfield, England, UK
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation. A copy of the license agreement is provided
 * in the file named "LICENSE.txt" included with this software distribution.
 * ----------------------------------------------------------------------------
 */

package inserm.converters.pfim;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.junit.After;

import com.csvreader.CsvReader;

import crx.converter.engine.ConversionDetail_;
import eu.ddmore.convertertoolbox.api.response.ConversionDetail;
import eu.ddmore.convertertoolbox.api.response.ConversionDetail.Severity;
import eu.ddmore.convertertoolbox.api.response.ConversionReport;
import eu.ddmore.convertertoolbox.api.response.ConversionReport.ConversionCode;

public abstract class TestBase {
	protected static final String PROGNAME_DIR = "../PFIM4.0/Program";
	
	protected boolean add_plotting_block = true;
	protected Converter c = null;
	protected boolean check_outputs = true;
	protected CsvReader data = null;
	protected File dir = null, f = null;
	protected boolean echo_exceptions = true;
	protected boolean execute_model = true;
	protected String [] expected_output_names = null;
	protected boolean fixedRunId = true, useCrxReportDetail = true;
	protected String inputXMLFile = null;
	protected String interpreter_path =  "R", command_format = "%s";
	protected RJob job = null;
	protected boolean lex_only = false;
	protected String output_directory = "../output", expected_output_file1 = null, expected_output_file2 = null, expected_output_file3 = null;
	protected String processed_pharmml_xml_filepath = "../output/out.xml";
	
	public TestBase() {
		dir = new File(output_directory);
		deleteAllFiles();
		job = new RJob();
	}
	
	protected void deleteAllFiles() {
		if(dir.exists()){
			for(File file: dir.listFiles())
				file.delete();
		}
	}
	
	protected void doJob() {
		assertTrue(dir.exists());
		assertTrue(f.exists());
		
		ConversionReport report = c.performConvert(f, dir);
		assertNotNull(report);
		assertEquals(ConversionCode.SUCCESS, report.getReturnCode());
		List<ConversionDetail> details = report.getDetails(Severity.INFO);
		assertFalse(details.isEmpty());
		assertNotNull(details.get(0));
		ConversionDetail_ detail = (ConversionDetail_) details.get(0);
		f = detail.getFile();
		assertTrue(f.exists());
		
		if (!execute_model) return;
		
		try {
			boolean result = job.run(dir, f);
			assertTrue(result);
		} catch (IOException e) {	
			fail("IOException");
		} catch (InterruptedException e) {
			echoStandardErrorFile();
			fail("InterruptedException");
		}
		
		if (!check_outputs) return;
		
		f = new File(expected_output_file1);
		assertTrue(f.exists()); 
		
		try {
			data = new CsvReader(expected_output_file1);
			boolean result = data.readHeaders();
			
			assertTrue(result);
			
			String [] cols = data.getHeaders();
			assertNotNull(cols);
			
			List<String> cols_ = new ArrayList<String>();
			for (String col : cols) cols_.add(col);
			
			for (String expected_output_name : expected_output_names) assertTrue(cols_.contains(expected_output_name));
			
			int count = 0;
			while (data.readRecord()) {
				String [] values = data.getValues();
				assertNotNull(values);
				assertEquals(values.length, cols.length);
				count++;
			}
			assertTrue(count > 0);
		} catch (FileNotFoundException e) {
			fail("FileNotFoundException");
		} catch (IOException e) {
			fail("IOException");
		}
	}
	
	protected void echoStandardErrorFile() {
		String error_file = output_directory + File.separator + "stderr.txt";
		File error = new File(error_file);
		if (error.exists()) {
			try {
				List<String> lines = new ArrayList<String>();
				BufferedReader in = new BufferedReader(new FileReader(error_file));
				String line = null;
				while( (line = in.readLine()) != null) lines.add(line);
				in.close();		
				
				if (!lines.isEmpty()) {
					System.err.println("R error messages:-");
					for (String line_ : lines) System.err.println(line_);
					System.err.println("========================\n");
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		} 
	}
	
	protected void init() throws NullPointerException, IOException {
		if(!dir.exists()) dir.mkdir();
		f = new File(inputXMLFile);
		c = new Converter();
		
		c.setAddPlottingBlock(false);
		Parser p = (Parser) c.getParser();
		p.setProgramDirectory(PROGNAME_DIR);
		
		job = new RJob();
		job.setInterpreterExePath(interpreter_path);
		job.setCommandFormat(command_format);
	}
	
	public abstract void setUp() throws Exception;
	
	@After
	public void tearDown() throws Exception {
		if (data != null) {
			data.close();
			data = null;
		}
	}
	
	public abstract void test();
}
