/* ----------------------------------------------------------------------------
 * This file is part of PharmML to PFIM 4.0 Converter.  
 * 
 * Copyright (C) 2016 jointly by the following organisations:- 
 * 1. INSERM, Paris, France
 * 2. Universite Paris Diderot, Paris, France
 * 3. EMBL European Bioinformatics Institute (EBML-EBI), Hinxton, UK
 * 4. Cyprotex Discovery Ltd, Macclesfield, England, UK
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation. A copy of the license agreement is provided
 * in the file named "LICENSE.txt" included with this software distribution.
 * ----------------------------------------------------------------------------
 */

package inserm.converters.pfim;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.IOException;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import crx.converter.engine.ConversionDetail_;
import eu.ddmore.convertertoolbox.api.response.ConversionDetail;
import eu.ddmore.convertertoolbox.api.response.ConversionDetail.Severity;
import eu.ddmore.convertertoolbox.api.response.ConversionReport;
import eu.ddmore.convertertoolbox.api.response.ConversionReport.ConversionCode;

public class Test_Simeoni_ShortWin2 extends TestBase {
	@Before
	public void setUp() throws Exception {
		inputXMLFile = "src/test/resources/Simeoni_Evaluation_20160603.xml";
		
		init();
	}

	@Test
	public void test() {
		assertTrue(dir.exists());
		assertTrue(f.exists());
		
		ConversionReport report = c.performConvert(f, dir);
		assertNotNull(report);
		assertEquals(ConversionCode.SUCCESS, report.getReturnCode());
		List<ConversionDetail> details = report.getDetails(Severity.INFO);
		assertFalse(details.isEmpty());
		assertNotNull(details.get(0));
		ConversionDetail_ detail = (ConversionDetail_) details.get(0);
		f = detail.getFile();
		assertTrue(f.exists());
		
		if (!execute_model) return;
		
		try {
			boolean result = job.run(dir, f);
			assertTrue(result);
		} catch (IOException e) {	
			fail("IOException");
		} catch (InterruptedException e) {
			echoStandardErrorFile();
			fail("InterruptedException");
		}
	}
}
