/* ----------------------------------------------------------------------------
 * This file is part of PharmML to PFIM 4.0 Converter.  
 * 
 * Copyright (C) 2016 jointly by the following organisations:- 
 * 1. INSERM, Paris, France
 * 2. Universite Paris Diderot, Paris, France
 * 3. EMBL European Bioinformatics Institute (EBML-EBI), Hinxton, UK
 * 4. Cyprotex Discovery Ltd, Macclesfield, England, UK
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation. A copy of the license agreement is provided
 * in the file named "LICENSE.txt" included with this software distribution.
 * ----------------------------------------------------------------------------
 */

package inserm.converters.pfim;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

import crx.converter.engine.ConversionDetail_;
import crx.converter.engine.common.Protocol;
import crx.converter.spi.blocks.TrialDesignBlock2;
import eu.ddmore.convertertoolbox.api.response.ConversionDetail;
import eu.ddmore.convertertoolbox.api.response.ConversionDetail.Severity;
import eu.ddmore.convertertoolbox.api.response.ConversionReport;
import eu.ddmore.convertertoolbox.api.response.ConversionReport.ConversionCode;
import eu.ddmore.libpharmml.dom.trialdesign.ArmDefinition;
import eu.ddmore.libpharmml.dom.trialdesign.Observation;

public class Test_Warafin_PKPD_Turnover extends TestBase {
	@Before
	public void setUp() throws Exception {
		inputXMLFile = "src/test/resources/design_warfarin_PKPD_turnover.xml";
		
		init();
	}

	@Test
	public void test() {
		assertTrue(dir.exists());
		assertTrue(f.exists());
		
		ConversionReport report = c.performConvert(f, dir);
		assertNotNull(report);
		assertEquals(ConversionCode.SUCCESS, report.getReturnCode());
		List<ConversionDetail> details = report.getDetails(Severity.INFO);
		assertFalse(details.isEmpty());
		assertNotNull(details.get(0));
		ConversionDetail_ detail = (ConversionDetail_) details.get(0);
		f = detail.getFile();
		assertTrue(f.exists());
		
		if (!execute_model) return;
		
		try {
			boolean result = job.run(dir, f);
			assertTrue(result);
		} catch (IOException e) {	
			fail("IOException");
		} catch (InterruptedException e) {
			echoStandardErrorFile();
			fail("InterruptedException");
		}
	}
	
	@Test
	public void test_LO() {
		assertTrue(dir.exists());
		assertTrue(f.exists());
		
		c.setLexOnly(true);
		ConversionReport report = c.performConvert(f, dir);
		assertNotNull(report);
		assertEquals(ConversionCode.SUCCESS, report.getReturnCode());
		
		TrialDesignBlock2 tdb = (TrialDesignBlock2) c.getTrialDesign();
		assertNotNull(tdb);
		
		List<Observation> obs = tdb.getObservations();
		assertNotNull(obs);
		assertFalse(obs.isEmpty());
		
		Map<Observation, List<ArmDefinition>> map = tdb.getArmMembershipMap();
		for (Observation ob : obs) {
			assertNotNull(ob);
			assertTrue(map.containsKey(ob));
		}
		
		String oid = "winPK";
		Observation winPK = tdb.getObservation(oid);
		assertNotNull(winPK);
		assertEquals(oid, winPK.getOid());
		
		List<ArmDefinition> arms = tdb.getArmMembership(winPK);
		assertNotNull(arms);
		assertFalse(arms.isEmpty());
		assertTrue(arms.size() == 20);
		
		// Test Expected identifiers
		List<String> oids = new ArrayList<String>();
		for (ArmDefinition arm : arms) {
			assertNotNull(arm);
			String oid_ = arm.getOid();
			assertNotNull(oid_);
			oids.add(oid_);
		}
		
		String format = "arm%s";
		for (int i = 1; i <= 20; i++) {
			String arm_oid = String.format(format, i);
			assertTrue(oids.contains(arm_oid));
		}
		
		oid = "winPD";
		Observation winPD = tdb.getObservation(oid);
		assertNotNull(winPD);
		assertEquals(oid, winPD.getOid());
		
		arms = tdb.getArmMembership(winPK);
		assertNotNull(arms);
		assertFalse(arms.isEmpty());
		assertTrue(arms.size() == 20);
		
		// Test Expected identifiers
		oids = new ArrayList<String>();
		for (ArmDefinition arm : arms) {
			assertNotNull(arm);
			String oid_ = arm.getOid();
			assertNotNull(oid_);
			oids.add(oid_);
		}
		
		for (int i = 1; i <= 20; i++) {
			String arm_oid = String.format(format, i);
			assertTrue(oids.contains(arm_oid));
		}
		
		List<Protocol> protocols = tdb.getProtocols();
		assertNotNull(protocols);
		assertTrue(protocols.size() == 2);
		
		int expected_size = 20;
		for (Protocol protocol : protocols) {
			assertNotNull(protocol);
			assertTrue(protocol.hasElementaryDesigns());
			assertTrue(expected_size == protocol.elementary_designs.size());
		}
	}
}
