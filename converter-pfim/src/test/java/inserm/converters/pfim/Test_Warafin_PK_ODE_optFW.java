/* ----------------------------------------------------------------------------
 * This file is part of PharmML to PFIM 4.0 Converter.  
 * 
 * Copyright (C) 2016 jointly by the following organisations:- 
 * 1. INSERM, Paris, France
 * 2. Universite Paris Diderot, Paris, France
 * 3. EMBL European Bioinformatics Institute (EBML-EBI), Hinxton, UK
 * 4. Cyprotex Discovery Ltd, Macclesfield, England, UK
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation. A copy of the license agreement is provided
 * in the file named "LICENSE.txt" included with this software distribution.
 * ----------------------------------------------------------------------------
 */

package inserm.converters.pfim;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.IOException;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import crx.converter.engine.ConversionDetail_;
import crx.converter.spi.blocks.TrialDesignBlock2;
import eu.ddmore.convertertoolbox.api.response.ConversionDetail;
import eu.ddmore.convertertoolbox.api.response.ConversionDetail.Severity;
import eu.ddmore.convertertoolbox.api.response.ConversionReport;
import eu.ddmore.convertertoolbox.api.response.ConversionReport.ConversionCode;
import eu.ddmore.libpharmml.dom.commontypes.OidRef;
import eu.ddmore.libpharmml.dom.trialdesign.Administration;
import eu.ddmore.libpharmml.dom.trialdesign.Observation;
import eu.ddmore.libpharmml.dom.trialdesign.SingleDesignSpace;

public class Test_Warafin_PK_ODE_optFW extends TestBase {
	@Before
	public void setUp() throws Exception {
		inputXMLFile = "src/test/resources/design_warfarin_PK_ODE_optFW.xml";
		
		init();
	}

	@Test
	public void test() {
		assertTrue(dir.exists());
		assertTrue(f.exists());
		
		ConversionReport report = c.performConvert(f, dir);
		assertNotNull(report);
		assertEquals(ConversionCode.SUCCESS, report.getReturnCode());
		List<ConversionDetail> details = report.getDetails(Severity.INFO);
		assertFalse(details.isEmpty());
		assertNotNull(details.get(0));
		ConversionDetail_ detail = (ConversionDetail_) details.get(0);
		f = detail.getFile();
		assertTrue(f.exists());
		
		if (!execute_model) return;
		
		try {
			boolean result = job.run(dir, f);
			assertTrue(result);
		} catch (IOException e) {	
			fail("IOException");
		} catch (InterruptedException e) {
			echoStandardErrorFile();
			fail("InterruptedException");
		}
	}
	
	@Test
	public void test_LO() {
		assertTrue(dir.exists());
		assertTrue(f.exists());
		
		c.setLexOnly(true);
		ConversionReport report = c.performConvert(f, dir);
		assertNotNull(report);
		assertEquals(ConversionCode.SUCCESS, report.getReturnCode());
		
		TrialDesignBlock2 tdb = (TrialDesignBlock2) c.getTrialDesign();
		assertNotNull(tdb);
		
		List<SingleDesignSpace> ds = tdb.getDesignSpaces();
		assertNotNull(ds);
		assertFalse(ds.isEmpty());
		assertTrue(ds.size() == 2);
		
		List<Administration> admins = tdb.getAdministrations();
		assertNotNull(admins);
		assertTrue(admins.size() == 1);
		
		String oid = "admin1";
		assertTrue(tdb.hasAdministration(oid));
		Administration admin = tdb.getAdministration(oid);
		assertNotNull(admin);
		assertEquals(oid, admin.getOid());
		
		oid = "admin2";
		assertFalse(tdb.hasAdministration(oid));
		admin = tdb.getAdministration(oid);
		assertNull(admin);
		
		OidRef ref = new OidRef("window1");
		Observation ob = tdb.getObservation(ref);
		assertNotNull(ob);
		assertEquals(ref.getOidRef(), ob.getOid());
		
		oid = "window1";
		for (SingleDesignSpace d : ds) {
			ob = tdb.getObservation(d);
			assertNotNull(ob);
			assertEquals(oid, ob.getOid());
		}
		
		SingleDesignSpace space = tdb.getDesignSpaceWithSamplingCountLimits();
		assertNotNull(space);
		assertTrue(tdb.hasDesignSpaceWithSamplingCountLimits());
	}
}
